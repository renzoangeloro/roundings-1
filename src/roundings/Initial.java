package roundings;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Initial {
	
	public Initial () {
		this.getDataFromUser();
	}
	
	public int recalculateRest(String denomination, String rest) {
		Integer newRest = 0;
		String number = "";
		int denominationNumber = Integer.parseInt(denomination);
		if(!(rest.equals("") && rest == null)) {
			if(rest.equals("0")) {
				return newRest;
			}else {
			List<Integer> descomp = new ArrayList<Integer>();
			for(int i = 0; i < rest.length(); i++) {
				descomp.add(rest.toCharArray()[i] - '0');
				number = number.concat(descomp.get(i).toString());
				if(descomp.size() == rest.length()) {
					int lastIndex = descomp.size() - 1;
					descomp.set(lastIndex, 0);
					number = number.substring(0,lastIndex);
					number = number.concat("0");
				}
			}}
			newRest = Integer.parseInt(number)+ denominationNumber;
			return newRest;
		}else {
			return newRest;
		}
	}
	
	public void returnMessage(Data data) {
		System.out.print("La denominacion es " + data.getDenomination() + " El vuelto es " + this.recalculateRest(data.getDenomination(), data.getRest()));
	}
	
	public void getDataFromUser() {
		Scanner entry = new Scanner(System.in);
		String denomination = "";
		String rest = "";
		Data dataFromUser = new Data();
		
		do {
			System.out.flush();
			System.out.print("Ingrese el vuelto: ");
			rest = entry.nextLine();
		}while(!isNumeric(rest));
		
		do {
			System.out.flush();
			System.out.print("Ingrese la denominacion: ");
			denomination = entry.nextLine();
		}while(!isNumeric(denomination));
		
		dataFromUser.setDenomination(denomination);
		dataFromUser.setRest(rest);
		this.returnMessage(dataFromUser);
	}
	
	public boolean isNumeric(String input) {
		try {
			double data = Integer.parseInt(input);
			if(data < 0) {
				System.out.println("El numero ingresado debe ser mayor a 0.");
				return false;
			}
			return true;
		}catch(NumberFormatException e) {
			System.out.println("Debe ser un dato numerico. ");
			return false;
		}
	}
}

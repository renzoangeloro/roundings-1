package roundings;

public class Data {
	
	String denomination;
	String rest;
	
	public Data (String denomination, String rest) {
		this.rest = rest;
		this.denomination = denomination;
	}
	
	public Data () {}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public String getRest() {
		return rest;
	}

	public void setRest(String rest) {
		this.rest = rest;
	}

	
}
